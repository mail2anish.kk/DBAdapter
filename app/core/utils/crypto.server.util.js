'use strict';
// another module- crypto-js
var crypto = require('crypto'),
  algorithm = 'aes-256-ctr',
  password = 'm0b1l3salt';

function encrypt(text) {
  console.log("in encrypt:");
  console.log("text=" + text);
  var cipher = crypto.createCipher(algorithm, password)
  var crypted = cipher.update(text, 'utf8', 'hex')
  crypted += cipher.final('hex');
  console.log("encrypt=" + crypted + " - text=" + text);
  return crypted;
}

function decrypt(text) {
  console.log("In decrypt:");
  console.log("Text to decrypt=" + text);
  var decipher = crypto.createDecipher(algorithm, password)
  var dec = decipher.update(text, 'hex', 'utf8')
  dec += decipher.final('utf8');
  console.log("dec=" + dec);
  return dec;
}

exports.encrypt = function(strToEncrypt) {
  return encrypt(strToEncrypt);
}

exports.decrypt = function(strToDecrypt) {
  return decrypt(strToDecrypt);
}
