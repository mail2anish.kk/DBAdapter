var oracledb = require('oracledb'),
  _ = require('lodash'),
  async = require('async'),
  logger = require('../utils/logger.server.util'),
  principal = require('../models/UserPrincipal');


function getConnection(tempStore, callBack) {
  var poolName = tempStore.config.appName,
    dbConfigs = tempStore.config.dataSourceConfig,
    oraPool;


  oraPool = require('../dao/dbconnect.server.dao')(poolName, dbConfigs, function(err, connection) {
    if (err) {
      logger.error(__filename, "ERROR:", err);
      return callBack(err);
    }
    return callBack(null, tempStore, connection);
  });
}

function doRelease(connection) {
  connection.release(
    function(err) {
      if (err) {
        logger.error(__filename, err.message);
      }
    });
}

function execProc(tempStore, connection, callBack) {
  var username = tempStore.username,
    password = tempStore.password;
  var codeBlock =
    "DECLARE  user_id VARCHAR2(100);  pidm spriden.spriden_pidm%TYPE ; pin          VARCHAR2(100);  pin_valid    VARCHAR2(100);  pin_expired  VARCHAR2(100);  pin_disabled VARCHAR2(100);  gobtpac_rec gobtpac%ROWTYPE;  lv_pinhash gobtpac.gobtpac_pin%TYPE;    CURSOR gobtpacpidm (spridenid varchar2)   IS      SELECT gobtpac_pidm FROM gobtpac, spriden WHERE gobtpac_pidm = spriden_pidm and spriden_id = spridenid and spriden_entity_ind = 'P' and spriden_change_ind is null; CURSOR gobtpacc (pidm NUMBER) RETURN gobtpac%ROWTYPE   IS   SELECT *    FROM gobtpac  WHERE gobtpac_pidm = pidm; BEGIN  user_id := :user_id;  pin    := :pin;  pin_valid    := 'E';  pin_expired  := 'Y';  pin_disabled := 'Y';  open gobtpacpidm(user_id);  fetch gobtpacpidm into pidm;  if gobtpacpidm%notfound then    close gobtpacpidm;    return;  end if;  close gobtpacpidm;  IF (pidm     IS NULL) OR (pin IS NULL) THEN    RETURN;  END IF;  OPEN gobtpacc (pidm);  FETCH gobtpacc INTO gobtpac_rec;  IF gobtpacc%NOTFOUND THEN    CLOSE gobtpacc;    RETURN;  END IF;  CLOSE gobtpacc;  pin_disabled                       := gobtpac_rec.gobtpac_pin_disabled_ind;  IF (gobtpac_rec.gobtpac_pin_exp_date >= SYSDATE) OR (gobtpac_rec.gobtpac_pin_exp_date IS NULL) THEN    pin_expired                      := 'N';  END IF;  lv_pinhash                := gb_third_party_access.F_GET_PINHASH(pidm, pin);  IF gobtpac_rec.gobtpac_pin = lv_pinhash THEN    pin_valid             := 'Y';  ELSE    pin_valid := 'N';  END IF;  IF (pin_valid = 'Y') AND ((pin_disabled = 'Y') OR (pin_expired = 'Y')) THEN    pin_valid  := 'X';  END IF; :pin_valid := pin_valid; :erpId := pidm; END;";
  connection.execute(
    codeBlock, {
      user_id: username,
      pin: password,
      pin_valid: {
        dir: oracledb.BIND_OUT,
        type: oracledb.STRING
      },
      erpId: {
        dir: oracledb.BIND_OUT,
        type: oracledb.NUMBER
      }
    }, {
      outFormat: oracledb.OBJECT,
      extendedMetaData: false
    },
    function(err, result) {
      if (err) {
        logger.error(_filename, "Error executing authentication SQL: ", err);
        return callBack(err, null);
      }
      var pinValid, erpId;
      if (result.outBinds) {
        pinValid = result.outBinds.pin_valid;
        erpId = result.outBinds.erpId;
      }
      return callBack(null, tempStore, pinValid,
        erpId, connection);
    });
}


module.exports = {
  authenticate: function authenticate(tempStore, callBack) {
    async.waterfall([
        async.apply(getConnection, tempStore),
        execProc
      ],
      function(err, tempStore, pinValid,
        erpId, connection) {
        //release connection parallely
        doRelease(connection);
        // Create uUserPrincipal Object and return
        var userPrincipal;
        if (pinValid == "Y") {
          userPrincipal = principal.newUserPrincipal(
            tempStore.username,
            erpId, erpId, '',
            tempStore.config.tenant.name,
            tempStore.config.appName);
          tempStore.userPrincipal = userPrincipal;
        }
        callBack(err, tempStore);
      });
  }
}
