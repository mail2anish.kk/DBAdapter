'use strict';
////////////////////////////////////////////////////////////////////////////////
//	Mobile Authentication Controller
//	authenticate : function is called from mobileauth.server.routes for all
//  auth requests from mobile users. It identifies the auth type and configs
//  and calls appropriate auth type services to authenticate.
//	The authentication status are passed back - auth token in case of success
//						or proper error message in case of auth failure
////////////////////////////////////////////////////////////////////////////////
var _ = require('lodash'),
	async = require('async'),
	logger = require('../utils/logger.server.util'),
	baseservice = require('../services/mobilebase.server.services'),
	ticketGen = require('../utils/ticketgen.server.util'),
	principal = require('../models/UserPrincipal'),
	cacheService = require('../services/cache.server.services'),
	utils = require('../utils/mobile.server.util');



function getTenantConfigs(tempStore, callBack) {
	cacheService.getCachedObject(tempStore.tenantApp + '.config', function(err, config) {
		if (err) {
			err.statuscode = 500;
			err.error = "Internal server error";
			return callBack(err);
		}
		if (!config || config == undefined) {
			// config not found in cache. Get config from database and add to cache
			// This scenario might occur when added config from backend or from outside app.
			utils.getTenantConfigFromDB(tempStore.tenantApp, function(err, tenantConfig) {
				if (err) {
					return callBack(err);
				}
				tempStore.config = tenantConfig;
				return callBack(null, tempStore);
			});
		} else {
			logger.debug(__filename, "- Tenant config found on cache for app:", tempStore.tenantApp);
			logger.debug(config);
			tempStore.config = config;
			return callBack(null, tempStore);
		}
	});
}

function authenticateUser(tempStore, callBack) {
	var authService;
	switch (tempStore.config.authType) {
		case "CAS":
			//CAS auth
			authService = require('../services/casauth.server.services');
			break;
		case "LDAP":
			//LDAP auth
			authService = require('../services/ldapauth.server.services');
			break;
		case "SSB":
			// SSB Auth
			authService = require('../services/nativeauth.server.services');
			break;
		default:
			//Error:
			break;
	}
	if (authService) {
		authService.authenticate(tempStore,
			function(err,
				tempStore) {
				var resOut;
				if (err) {
					var errCode, errRes;
					errCode = (err.statuscode) ? err.statuscode : 500;
					errRes = (err.error) ? err.error : JSON.stringify(err);
					return callBack(err);
				}
				// check if UserPrincipal object is valid
				var userPrincipal = tempStore.userPrincipal;
				if (userPrincipal && userPrincipal.username) {
					return callBack(null, tempStore);
				} else {
					var err = {
						statuscode: 200,
						error: "Invalid username/password"
					};
					return callBack(err);
				}
			});
	} else {
		var err = {
			statuscode: 200,
			error: "Tenant not found"
		};
		return callBack(err);
	}
}

function invalidateOldTickets(tempStore, callBack) {
	return callBack(null, tempStore);
	/* We do not invalidate old tickets in the new db adapter:
	var userPrincipal = tempStore.userPrincipal;
	utils.invalidateOldTicket(userPrincipal, function(err) {
		callBack(null, tempStore);
	});
	*/
}

function generateAuthTicket(tempStore, callBack) {
	// Generate auth ticket
	var ticket = ticketGen();
	tempStore.ticket = ticket;
	return callBack(null, tempStore);
}

function addUserInfoToCache(tempStore, callBack) {
	var userPrincipal = tempStore.userPrincipal;
	var config = tempStore.config;
	var ticket = tempStore.ticket;
	var cacheKey = config.appName + ".ticket." + ticket;
	cacheService.addToCache(cacheKey, userPrincipal, config, function(err, success) {
		if (err) {
			var err = {
				statuscode: 500,
				error: "Internal server error"
			};
			return callBack(err);
		}
		return callBack(null, tempStore);
	});
}
////////////////////////////////////////////////////////////////////////////
///  Global Controller functions
////////////////////////////////////////////////////////////////////////////
module.exports = {
	authenticate: function processAuthenticationRequest(req, res) {
		//console.log(req.method + " : tenantApp=" + req.params.tenantapp + " : " + req.url);
		logger.info(__filename, "- Authenticating user ", req.body.username, "using URL", req.url);
		var tempStore = {},
			config,
			tenantApp = req.params.tenantapp,
			username = req.body.username,
			password = decodeURIComponent(req.body.password),
			outResponse;
		if (!username) {
			res.writeHead(500, {
				'Content-Type': 'application/json'
			});
			outResponse = {
				"error": "Username cannot be blank !"
			}
			res.end(JSON.stringify(outResponse));
			return;
		}
		if (tenantApp) {
			tempStore.tenantApp = tenantApp;
			tempStore.username = username;
			tempStore.password = password;
			async.waterfall([
				async.apply(getTenantConfigs, tempStore),
				authenticateUser,
				//invalidateOldTickets,
				generateAuthTicket,
				addUserInfoToCache
			], function(err, tempStore) {
				if (err) {
					err.statuscode = (err.statscode) ? err.statuscode : 500;
					res.writeHead(err.statuscode, {
						'Content-Type': 'application/json'
					});
					outResponse = {
						"error": err.error
					}
					res.end(JSON.stringify(outResponse));
					return;
				}
				var userPrincipal = tempStore.userPrincipal;

				// Send ticket as response
				res.writeHead(200, {
					'Content-Type': 'application/json'
				});
				outResponse = {
					"ticket": tempStore.ticket,
					"roles": (userPrincipal.roles && userPrincipal.roles.length > 0) ? userPrincipal.roles : null
				}
				res.end(JSON.stringify(outResponse));
			});
		} else {
			//Error: Auth URL problem: not getting Appname
			res.writeHead(500, {
				'Content-Type': 'application/json'
			});
			outResponse = {
				"error": "Tenant not found"
			}
			res.end(JSON.stringify(outResponse));
		}
		//return;
	}
}
