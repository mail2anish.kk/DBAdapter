'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var FeatureSchema = new Schema({
    tenant: {
        type: String,
        required: true
    },
    appname: {
        type: String,
        required: true
    },
    featurename: {
        type: String,
        required: true
    },
    enabled: {
        type: Boolean,
        default: true
    },
    services: [String],
    roles: [String],
    grants: [{
        objectName: String,
        privileges: [String]
    }]
});

FeatureSchema.statics.findFeaturesByTenantApp = function(appname, callBack) {
   console.log("FeatureSchema.appname="+appname);
    this.find({
        appname: appname
    }, callBack);
}

module.exports = mongoose.model("Feature", FeatureSchema, "Feature");
