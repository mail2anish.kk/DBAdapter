'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var CacheSchema = new Schema({
  key: {
    type: String,
    required: true
  },
  lastUpdated: {
    type: Date,
    default: Date.now
  },
  data: {}
});


CacheSchema.pre('save', function(next) {
  //console.log("in pre save schema");
  this.lastUpdated = Date.now;

  next();
});

CacheSchema.statics.listCache = function(callBack) {
  this.findAll({}, callBack);
}

CacheSchema.statics.findCache = function(keyText, callBack) {
  console.log("in cacheschema:");
  console.log("tikcet:" + keyText);

  this.findOne({
    key: keyText
  }, callBack);
}

CacheSchema.statics.addtoCache = function(keyText, valObject, callBack) {
  var cacheRow = new this()
  cacheRow.key = keyText;
  cacheRow.data = valObject;
  cacheRow.save(callBack);
}

CacheSchema.statics.removeFromCache = function(keyText, callBack) {
  this.remove({
    key: keyText
  }, callBack);
}

module.exports = mongoose.model("CacheSchema", CacheSchema, "CacheSchema");
