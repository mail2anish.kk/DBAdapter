'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


var TenantSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  longName: {
    type: String,
    required: true
  },
  email: String
});


TenantSchema.statics.listTenants = function(callBack) {
  this.findAll({}, callBack);
}

TenantSchema.statics.findTenant = function(tenantName, callBack) {
  this.findOne({
    name: tenantName
  }, callBack);
}

module.exports = mongoose.model("Tenant", TenantSchema, "Tenant");
