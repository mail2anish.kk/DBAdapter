'use strict';

var async = require('async'),
  _ = require('lodash'),
  mongoose = require('mongoose'),
  logger = require('../../core/utils/logger.server.util'),
  cacheService = require('../../core/services/cache.server.services'),
  tenant = require('../models/Tenant.server.model'),
  productConfig = require('../models/ProductConfig.server.model'),
  features = require('../utils/feature.server.util');



function getTenantCachedTicketKeys(callBack) {
  cacheService.filterKeysContainString('.ticket.', function(err, values) {
    if (err) {
      logger.error("Error getting ticket keys from cache:" + err);
      return callBack(err);
    }
    //console.log("ticket keys ");
    //console.log(values);
    return callBack(null, values)
  });

}

function validateTicketTime(tickets, callBack) {
  var dFrom, dTo, dMins = 0;
  async.each(tickets,
    function(ticket, callBack) {
      cacheService.getCachedObject(ticket, function(err, userPrincipal) {
        if (!err && userPrincipal) {
          dFrom = new Date(userPrincipal.lastUpdated);
          dTo = new Date();
          dMins = (dTo.getTime() - dFrom.getTime()) / 60000;
          logger.info("Ticket : " + ticket + " in cache for " + dMins + "mins.");
          if (dMins > 5) {
            //Remove ticket from cache
            cacheService.removeFromCache(ticket, function(err, count) {
              if (!err) {
                logger.info(count + " ticket removed from cache.");
              }
              return callBack();
            });
          } else {
            return callBack();
          }
        } else {
          return callBack();
        }
      });
    },
    function(err) {
      return callBack();
    }
  )
}

exports.initializeTenants = function(callBack) {
  // Get the Tenant Configurations from database
  productConfig.find({})
    .populate('tenant')
    .exec(function(err, tenantConfigs) {
      if (err) {
        return callBack(err);
      }
      // Cache tenant configurations for each tenant configuration
      logger.info(__filename, "-", tenantConfigs.length, "tenant configurations loaded.");
      async.each(tenantConfigs,
        function(tenantConfig, cb) {
          logger.info(__filename, "- Configuration for app:", tenantConfig.appName,
            "loading to cache... ");
          logger.info(JSON.stringify(tenantConfig));
          cacheService.addToCache(tenantConfig.appName + '.config',
            JSON.parse(JSON.stringify(tenantConfig)),
            '',
            function(err, success) {
              if (!err) {
                //logger.info("cached config for " + tenantConfig.appName);
                // cache the features for the tenant (if not exists)
                // loadFeatureServices is called in parallel to save app loading time
                /* This feature is disabled as we may not use this.
                features.loadFeatureServices(
                    tenantConfig,
                    function(err, success) {
                        if (!err) {
                            console.log(
                                "Services for " +
                                tenantConfig
                                .appName +
                                " added successfully !"
                            );
                        }
                    });
                    */
                //
                //check cached tenantapp
                /*
                console.log("printing cache again for tenant " + tenantConfig.appName);
                cacheService.getCachedObject(tenantConfig.appName + '.config', function(err, value) {
                  console.log("printing cache again for tenant " + tenantConfig.appName);
                  if (err) {
                    console.log("Error getting config from cache:" + err);
                    return;
                  }
                  console.log(value);
                });
                */
              }
              return cb(err, success);
            });
        },
        function(err) {
          if (err) {
            logger.error(__filename,
              " - Error while caching the tenant configurations:"
            );

            logger.error(err);
            return callBack(err);
          }
          return callBack(err, tenantConfigs);
        });
    });
}

exports.clearExpiredTickets = function(callBack) {
  async.waterfall([
    getTenantCachedTicketKeys,
    validateTicketTime
  ], function(err, tKeys) {
    return callBack();
  });

}
