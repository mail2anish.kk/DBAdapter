angular.module('appModule')
  .controller('TenantDetailController', function($scope, $rootScope, $http, $location, $route,
    $routeParams) {
    console.log("in tenant detail controller");

    var id = $routeParams.tenantid;
    console.log($routeParams);

    $scope.errorMessage = "";
    $rootScope.getAPI('/api/v1/tenants/' + id, function(err, response) {
      if (err) {
        if (err.status == 403) {
          $location.path('/');
        }
        $scope.errorMessage = "Tenant not found !";
        return;
      }
      $scope.newTenant = response.data;
      console.log($scope.newTenant);
    });


  });
