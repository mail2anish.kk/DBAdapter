angular.module('appModule')
  .controller('TenantDeleteController', function($scope, $rootScope, $http, $location, $route,
    $routeParams) {
    console.log("in tenant delete controller");

    var id = $routeParams.tenantid;
    console.log($routeParams);

    $scope.deleteTenant = function() {
      $rootScope.deleteAPI('/api/v1/tenants/' + id, function(
        err, response) {
        if (err) {
          console.log(err);
          $scope.errorMessage = error.data.message;
          return;
        }
        console.log(response);
        $location.path('/admin/tenant/list');
      });
    }

    $scope.cancelAction = function() {
      $location.path('/admin/tenant/list');
    }

    if (confirm('Are you sure you want to delete this tenant?')) {
      $scope.deleteTenant();
    } else {
      $scope.cancelAction();
    }

  });
