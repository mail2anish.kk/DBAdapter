(function() {
  angular.module('appModule', ['ngRoute'])
    .run(['$http', '$rootScope', '$window', function($http, $rootScope, $window) {
      // $rootScope.getAPI
      $rootScope.getAPI = function(url, cb) {
          var token = $window.localStorage.getItem("token");
          if (token) {
            $rootScope.token = token;
            $rootScope.user = $window.localStorage.getItem("user");
            var header = {
              headers: {
                "Authorization": $rootScope.token
              }
            }
            $http.get(url, header).then(function(response) {
              console.log("response:");
              console.log(response);
              if (response.status == 403) {
                $window.localStorage.removeItem("token");
                $window.localStorage.removeItem("user");
                $rootScope.token = "";
                $rootScope.user = "";
              }
              return cb(null, response);
            }, function(error) {
              //$window.localStorage.removeItem("token");
              //$window.localStorage.removeItem("user");
              //$rootScope.token = "";
              //$rootScope.user = "";
              console.log(error);
              return cb(error);
            });
          } else {
            return cb({
              "error": "Not authenticated"
            });
          }
        }
        //$rootScope.putAPI
      $rootScope.putAPI = function(url, data, cb) {
          var token = $window.localStorage.getItem("token");
          if (token) {
            $rootScope.token = token;
            $rootScope.user = $window.localStorage.getItem("user");
            var header = {
              headers: {
                "Authorization": $rootScope.token
              }
            }
            $http.put(url, data, header).then(function(response) {
              console.log("response:");
              console.log(response);
              if (response.status == 403) {
                $window.localStorage.removeItem("token");
                $window.localStorage.removeItem("user");
                $rootScope.token = "";
                $rootScope.user = "";
              }
              return cb(null, response);
            }, function(error) {
              //$window.localStorage.removeItem("token");
              //$window.localStorage.removeItem("user");
              //$rootScope.token = "";
              //$rootScope.user = "";
              console.log(error);
              return cb(error);
            });
          } else {
            return cb({
              "error": "Not authenticated"
            });
          }
        }
        //$rootScope.postAPI
      $rootScope.postAPI = function(url, data, cb) {
          var token = $window.localStorage.getItem("token");
          if (token) {
            $rootScope.token = token;
            $rootScope.user = $window.localStorage.getItem("user");
            var header = {
              headers: {
                "Authorization": $rootScope.token
              }
            }
            $http.post(url, data, header).then(function(response) {
              console.log("response:");
              console.log(response);
              if (response.status == 403) {
                $window.localStorage.removeItem("token");
                $window.localStorage.removeItem("user");
                $rootScope.token = "";
                $rootScope.user = "";
              }
              return cb(null, response);
            }, function(error) {
              //$window.localStorage.removeItem("token");
              //$window.localStorage.removeItem("user");
              //$rootScope.token = "";
              //$rootScope.user = "";
              console.log(error);
              return cb(error);
            });
          } else {
            return cb({
              "error": "Not authenticated"
            });
          }
        }
        // $rootScope.deleteAPI
      $rootScope.deleteAPI = function(url, cb) {
        var token = $window.localStorage.getItem("token");
        if (token) {
          $rootScope.token = token;
          $rootScope.user = $window.localStorage.getItem("user");
          var header = {
            headers: {
              "Authorization": $rootScope.token
            }
          }
          $http.delete(url, header).then(function(response) {
            console.log("response:");
            console.log(response);
            if (response.status == 403) {
              $window.localStorage.removeItem("token");
              $window.localStorage.removeItem("user");
              $rootScope.token = "";
              $rootScope.user = "";
            }
            return cb(null, response);
          }, function(error) {
            console.log(error);
            return cb(error);
          });
        } else {
          return cb({
            "error": "Not authenticated"
          });
        }
      }
    }])
    .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
      $routeProvider
        .when("/", {
          templateUrl: "login.html",
          controller: "LoginController"
        })
        .when("/login", {
          templateUrl: "login.html",
          controller: "LoginController"
        })
        .when("/home", {
          templateUrl: "home.html"
        })
        .when("/admin/tenant/list", {
          templateUrl: "views/tenant/tenant_list.html",
          controller: "TenantListController"
        })
        .when("/admin/tenant/show/:tenantid", {
          templateUrl: "views/tenant/tenant_show.html",
          controller: "TenantDetailController"
        })
        .when("/admin/tenant/create", {
          templateUrl: "views/tenant/tenant_create.html",
          controller: "TenantCreateController"
        })
        .when("/admin/tenant/edit/:tenantid", {
          templateUrl: "views/tenant/tenant_edit.html",
          controller: "TenantEditController"
        })
        .when("/admin/tenant/delete/:tenantid", {
          templateUrl: "views/tenant/tenant_delete.html",
          controller: "TenantDeleteController"
        })
        .when("/admin/tenant/:tenantid/configuration/show/:configid", {
          templateUrl: "views/config/config.html",
          controller: "TenantConfigController"
        })
        .when("/admin/tenant/:tenantid/configuration/create", {
          templateUrl: "views/config/config.html",
          controller: "TenantConfigController"
        })
        .otherwise({
          template: "<h1>No page loaded</h1>"
        })
        //$locationProvider.html5Mode(true);
    }]);


})();
